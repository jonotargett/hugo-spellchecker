# Hugo_SpellChecker

A script to automatically run a spellchecker across all Markdown content files for a site using the Hugo SSG.
Uses the [GrammarBot](https://www.grammarbot.io/) API to run a check on the plaintext version of all of your
page content.

[![Screenshot](spellcheck.png)](https://gitlab.com/jonotargett/hugo-spellchecker)

A list of issues in each file will be printed to the console as the script is run. This
list includes the approximate location, type of issue (spelling, grammar, repeated word) and possible replacements.
Additionally, the JSON output is stored for each file to allow in-browser highlighing on your published content.

The hash of each content file is also stored alongside the JSON response to prevent spellcheck from being
run on files which haven't changed since the last pass. This keeps the publishing process fast if you
include this into your site build process. It also reduces the number of API calls the GrammarBot.

## Usage

Call the following line inside whatever build script you are running, prior to calling ```hugo```.
```shell
make CONTENT_DIR=<path/to/hugo/content> OUTPUT_DIR=<path/to/hugo/static>
```

This will place all of the *.json files for all of your content documents into `<project root>/static/spellchecker```,
as well as including the CSS and JS files necessary to provide in browser highlighting.

### In-browser highlighting
To enable the in-browser highlighting, you'll need to include the following into each page that will
have spellcheck'd conent. Easiest is to paste this into a ```footer.html``` template.

```html
{{/* hugo-spellchecker */}}
<script src="{{ .Site.BaseURL }}js/spellcheck.js" async></script>
<link rel="stylesheet" type="text/css" href="{{ .Site.BaseURL }}css/tooltips.css" />
```

Issue highlighting is disabled by default so that the spelling tips don't interfere with regular formatting
or your regular user's experience.
The script can be enabled by requesting the page ```[page.html]?spellcheck```, either by providing
a link within the page or just by copypasting into the URL bar. It won't cause any conflict with any 
other query parameters, it just checks if the spellcheck variable is present.

