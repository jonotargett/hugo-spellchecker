# todo


ifndef $(CONTENT_DIR)
CONTENT_DIR=./content
endif
ifndef $(OUTPUT_DIR)
OUTPUT_DIR=./output
endif

RESOURCES_DIR=./resources


make:
	@echo "Processing directory $(CONTENT_DIR)"
	@echo "Sending output to $(OUTPUT_DIR)"

	@echo "Creating folders..."
	@mkdir -p $(OUTPUT_DIR)
	@mkdir -p $(OUTPUT_DIR)/spellcheck
	@echo "Copying resources..."
	@cp -r $(RESOURCES_DIR)/* $(OUTPUT_DIR)

	@echo "Running spellchecker..."
	./spellchecker.py $(CONTENT_DIR) $(OUTPUT_DIR)/spellcheck


clean:
	rm -rf $(OUTPUT_DIR)/spellcheck
