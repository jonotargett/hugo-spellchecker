#! /usr/bin/env python3

# Did I deliberately align these in a nice cascade?
# Yes. Yes I did.
import os
import sys
import glob
import json
import hashlib
import requests
from io import StringIO
from markdown import Markdown


# Terminal output colours
class Colors:
	NORMAL =  '\033[0m'
	RED =     '\033[31m'
	GREEN =   '\033[32m'
	YELLOW =  '\033[33m'
	BLUE =    '\033[34m'
	MAGENTA = '\033[35m'
	CYAN =    '\033[36m'

# Creating a function to patch into the markdown parser to get plaintext output
# from https://stackoverflow.com/a/54923798
def unmark_element(element, stream=None):
	if stream is None:
		stream = StringIO()
	if element.text:
		stream.write(element.text)
	for sub in element:
		unmark_element(sub, stream)
	if element.tail:
		stream.write(element.tail)
	return stream.getvalue()

# patching Markdown with our new function
Markdown.output_formats["plain"] = unmark_element
__md = Markdown(output_format="plain")
__md.stripTopLevelTags = False


def content_list(directory):
	# find all markdown files within a directory and all subdirectories
	search_dir = directory + '/'
	pref_len = len(search_dir)
	return [ filename[(filename.find(search_dir)+pref_len):] \
		for filename in glob.iglob(search_dir + '/**/*.md', recursive = True) ]


def get_markdown_content(file):
	# return just the raw markdown from a file, stripping anything between '+++' lines.
	# these are assumed to be the headers that the hugo templater uses for document metadata
	# also ignores anything in the document body that looks like it is between ``` code tags.
	file_contents = ''
	currently_in_ignored_block = False

	with open(file, 'r') as file:
		for line in file:
			if line.strip() == '+++' or line.strip()[0:3] == '```':
				currently_in_ignored_block = not currently_in_ignored_block
			else:
				if not currently_in_ignored_block:
					file_contents += line

	return file_contents


def get_grammar_report(content):
	headers = {
    'Content-Type': 'application/json',
	}
	data = {
	  'api_key': 'AF5B9M2X',
	  'language': 'en-AU',
	  'text': content
	}
	response = requests.post(
		'http://api.grammarbot.io/v2/check',
		headers=headers,
		data=data)

	if response.status_code == 200:
		return json.loads(response.text)
	else:
		print('Error: response code', response.status_code)
		return {}	# return an empty dictionary instead of None, so that JS can read it properly


if __name__ == '__main__':
	print('\nStarting Markdown Spell Checker')

	content_dir = 'content'
	output_dir = 'static/spellcheck'

	if len(sys.argv) == 3:
		content_dir = sys.argv[1]
		output_dir = sys.argv[2]

	# Build a custom dictionary of known words so that we can ignore when the spellchecker gets
	# angry about brand names
	acceptable_words = []
	if os.path.isfile('words'):
		with open('words', 'r') as infile:
			for line in infile:
				if line.strip() != "":
					acceptable_words.append(line.strip())

		# Sort the dictionary and write it back to the file, just to be helpful
		acceptable_words = sorted(acceptable_words, key=str.lower)
		with open('words', 'w+') as outfile:
			for item in acceptable_words:
				outfile.write("%s\n" % item)

	print(' - Scanning all files in', content_dir, '\n - Output directory is', output_dir, '\n')
	files = content_list(content_dir)


	for file in files:
		fullpath = content_dir + '/' + file
		outpath = output_dir + '/' + file + '.json'

		# turn markdown into plaintext
		markdown = get_markdown_content(fullpath)
		plaintext = __md.convert(markdown)

		# generate a hash of the file
		hasher = hashlib.sha1()
		hasher.update(markdown.encode('utf-8')) # hash the original content: including headers
		hash = hasher.hexdigest()


		# prepare our data structure. If there is any spellcheck data already written to file,
		# load that in so that we can check if it is current.
		data = {}
		data['filehash'] = None
		data['spellcheck'] = {}

		if os.path.isfile(outpath):
			with open(outpath, 'r') as infile:
				data = json.load(infile)


		# if the file has changed since last checked, or if there is no spellcheck data saved
		# (which can occur if the GrammarBot API refuses our request)
		if data['filehash'] != hash or not data['spellcheck']:
			print(file + ': ' + Colors.CYAN + 'RUNNING' + Colors.NORMAL)

			# update the data pack with the current info for the file
			data['filehash'] = hash
			report = get_grammar_report(plaintext)

			print(report)
			data['spellcheck']['software'] = report['software']
			data['spellcheck']['warnings'] = report['warnings']
			data['spellcheck']['language'] = report['language']
			data['spellcheck']['matches'] = []

			plaintext_length = len(plaintext)

			# Print any warnings in the console.
			for error in report['matches']:
				text = error['context']['text']
				offset = int(error['context']['offset'])
				length = int(error['context']['length'])

				if text[offset:offset+length] in acceptable_words:
					continue
				else:
					data['spellcheck']['matches'].append(error)

				# Get data out of the GrammarBot struct into meaningful chunks
				location = round(float(error['offset']) * 100 / plaintext_length)
				message = error['shortMessage']
				replacements = ', '.join([ ('"' + x['value'] + '"') \
					for x in error['replacements'] if len(error['replacements']) < 10 ])

				# If the warning has any helpful fix suggestions, format a nice message
				if message:
					message = Colors.RED + message + Colors.NORMAL + '. '
					if replacements:
						message += 'Consider ' + replacements

				print( '\t@ ' + str(location) + '%: ' \
					+ message + '\n\t' \
					+ text[:offset] + Colors.YELLOW \
					+ text[offset:offset+length] + Colors.NORMAL \
					+ text[offset+length:])

			# Ensure that the output path exists for the .json, then write to file
			os.makedirs(os.path.dirname(outpath), exist_ok=True)
			with open(outpath, 'w+') as outfile:
				json.dump(data, outfile, indent=2)

		else:
			# If the file hasn't changed since last check, just print a message so that we
			# know it didn't get skipped.
			print(file + ': ' + Colors.GREEN + 'DONE' + Colors.NORMAL)

#<end __main__












# Does anyone else keep a little code graveyard sitting at the back end of their source files?
'''
if search_dir[0] is not '/':
	# if the directory served isn't a full path, prepend the working directory
	cwd = os.getcwd()
	search_dir = cwd + '/' + directory
'''
















