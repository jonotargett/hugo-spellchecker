

// modified from https://stackoverflow.com/a/2091331
function hasQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return true;
        }
    }
    return false;
}


// https://stackoverflow.com/a/7524621
function getMeta(metaName) {
  const metas = document.getElementsByTagName('meta');

  for (let i = 0; i < metas.length; i++) {
    if (metas[i].getAttribute('name') === metaName) {
      return metas[i].getAttribute('content');
    }
  }
  return '';
}


function getSpellCheckData(baseUrl, spellingFile) {
  
    var x = new XMLHttpRequest();
    var thisPtr = this;
    x.open('GET', baseUrl + spellingFile);
    x.onreadystatechange = function() {
        if (x.readyState == 4 && /^2|0/.test(x.status)) {
            tagSpellingMistakes(x.responseText);
        }
    };
    x.send(null);
}


function tagSpellingMistakes(spellingData) {
  data = JSON.parse(spellingData);
  
  issues = data.spellcheck.matches;
  var container = document.body
  
  for(var i = 0; i < issues.length; i += 1) {
    var context = issues[i].context.text
    var offset = parseInt(issues[i].context.offset)
    var length = parseInt(issues[i].context.length)
    var message = issues[i].message
    
    var replacements = ''
    if(issues[i].replacements.length == 1) {
      replacements = '"' + issues[i].replacements[0].value + '"'
    }
    if(issues[i].replacements.length > 1) {
      replacements = '"' + issues[i].replacements[0].value + '", or "' + issues[i].replacements[1].value + '"'
    }
    
    context = context.substring(offset, offset + length)
    
    wrapText(container, context, message, replacements)
  }
}


// modified from https://stackoverflow.com/a/1836463
function wrapText(container, context, message, replacements) {
  // Construct a regular expression that matches text at the start or end of a string or surrounded by non-word characters.
  // Escape any special regex characters in text.
  var textRE = new RegExp('(^|\\W)' + context.replace(/[\\^$*+.?[\]{}()|]/, '\\$&') + '($|\\W)', 'm');
  var nodeText;
  var nodeStack = [];

  // Remove empty text nodes and combine adjacent text nodes.
  container.normalize();

  // Iterate through the container's child elements, looking for text nodes.
  var curNode = container.firstChild;

  while (curNode != null) {
    if (curNode.nodeType == Node.TEXT_NODE) {
      // Get node text in a cross-browser compatible fashion.
      if (typeof curNode.textContent == 'string')
        nodeText = curNode.textContent;
      else
        nodeText = curNode.innerText;

      // Use a regular expression to check if this text node contains the target text.
      var match = textRE.exec(nodeText);
      if (match != null) {
        // Create a document fragment to hold the new nodes.
        var fragment = document.createDocumentFragment();

        // Create a new text node for any preceding text.
        if (match.index > 0)
          fragment.appendChild(document.createTextNode(match.input.substr(0, match.index)));

        // Create the wrapper span and add the matched text to it.
        var spanNode = document.createElement('span');
        spanNode.className = 'spelltip';
        spanNode.appendChild(document.createTextNode(match[0]));
        var spellingTipNode = document.createElement('span');
        spellingTipNode.className = 'spelltiptext';
        spellingTipNode.appendChild(document.createTextNode(message));
        if(replacements) {
          spellingTipNode.appendChild(document.createElement('br'));
          spellingTipNode.appendChild(document.createTextNode(replacements + '?'));
        }
        
        spanNode.appendChild(spellingTipNode);
        
        fragment.appendChild(spanNode);

        // Create a new text node for any following text.
        if (match.index + match[0].length < match.input.length)
          fragment.appendChild(document.createTextNode(match.input.substr(match.index + match[0].length)));

        // Replace the existing text node with the fragment.
        curNode.parentNode.replaceChild(fragment, curNode);

        curNode = spanNode;
      }
    } else if (curNode.nodeType == Node.ELEMENT_NODE && curNode.firstChild != null) {
      nodeStack.push(curNode);
      curNode = curNode.firstChild;
      // Skip the normal node advancement code.
      continue;
    }

    // If there's no more siblings at this level, pop back up the stack until we find one.
    while (curNode != null && curNode.nextSibling == null)
      curNode = nodeStack.pop();

    // If curNode is null, that means we've completed our scan of the DOM tree.
    // If not, we need to advance to the next sibling.
    if (curNode != null)
      curNode = curNode.nextSibling;
  }
}



// script entry point here

if(hasQueryVariable('spellcheck')) {
  var sourcePath = getMeta('page-source-file');
  var baseUrl = getMeta('site-base-url');
  
  if(!sourcePath || !baseUrl) {
    alert('No meta-info specified for spelling information');
  }
  else {
    spellingPath = 'spellcheck/' + sourcePath + '.json';
    //alert(sourcePath + '\n' + spellingPath);
    getSpellCheckData(baseUrl, spellingPath);
  }
}

